# Infoclimat

This project has been developed during the Hackathon on June 2021

## Technologies

-   GraphQL
-   ReactJS
-   MaterialUI

## Architecture

The project is divided into two parts. On the one hand the API part under the `/api` folder and on the other hand the client part under the `/client` folder

## Installation

1. Start back end

```bash
cd api && npm i && npm start
```

1. Start front end

```bash
cd front && npm i && npm start
```

## Contributors

-   Alix Jacobe De Haut
-   Julien Le Goanvic
-   Samuel Benitah
-   Ilan Ducret
-   Charles Van Hamme

# Infoclimat

This project has been developed during the Hackathon on June 2021

## Technologies

-   GraphQL
-   ReactJS
-   MaterialUI

## Architecture

The project is divided into two parts. On the one hand the API part under the `/api` folder and on the other hand the client part under the `/client` folder

## Installation

1. Start back end

```bash
cd api && npm i && npm start
```

1. Start front end

```bash
cd front && npm i && npm start
```

## Contributors

-   Alix Jacobe De Haut
-   Julien Le Goanvic
-   Samuel Benitah
-   Ilan Ducret
-   Charles Van Hamme
