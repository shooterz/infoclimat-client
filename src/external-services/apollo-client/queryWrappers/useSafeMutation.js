import { useMutation } from '@apollo/client';

import useQueryErrors from './useQueryErrors';

const useSafeMutation = (query, queryOptions, errorOptions) => {
  const [cb, { error, ...useQueryResult }] = useMutation(query, queryOptions);
  useQueryErrors(error, errorOptions);

  return [cb, { error, ...useQueryResult }];
};

export default useSafeMutation;
