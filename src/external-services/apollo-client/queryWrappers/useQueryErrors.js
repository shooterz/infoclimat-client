import styled from '@emotion/styled';
import { useUser } from 'business/loginAndRightsManagement/UserContext';
import { Paragraph1Strong, Paragraph2 } from 'components/Text';
import { logger } from 'external-services/datadog-logs';
import { useModalAcknowledge } from 'Layout/ModalAcknowledgeContext';
import React, { useCallback, useEffect } from 'react';
import { toast } from 'react-toastify';
import routes from 'routes';

import wording from './wording.json';

const RetryLater = styled(Paragraph2)`
  margin-top: 32px;
`;
const SupportWarned = styled(Paragraph2)`
  color: ${({ theme }) => theme.mainColorLight3};
  margin-top: 6px;
`;

const cleanLSAndReload = () => {
  localStorage.clear();
  window.location = routes.home.path;
};

const useQueryErrors = (
  error,
  { silentError = false, expectedErrors } = {}
) => {
  const { expireUserToken } = useUser();
  const { openAcknowledgeModal } = useModalAcknowledge();

  const openCleanModal = useCallback(
    () =>
      openAcknowledgeModal({
        title: wording.requestHeaderTooLargeTitle,
        confirmationText: (
          <>
            <Paragraph1Strong>{wording.needDisconnection}</Paragraph1Strong>
            <RetryLater>{wording.retryLater}</RetryLater>
            <SupportWarned>{wording.supportWarned}</SupportWarned>
          </>
        ),
        onConfirmText: wording.disconnect,
        onConfirm: cleanLSAndReload,
        type: 'error',
      }),
    [openAcknowledgeModal]
  );

  /**
   * The clean way to handle authentication errors would be to return a custom http
   * error code on server side (ex : 498)
   * This should be possible through plugins https://github.com/apollographql/apollo-server/issues/1709#issuecomment-495793375
   * Unfortunately it not because of https://github.com/apollographql/apollo-server/issues/3223
   * So in the meantime we use isAuthenticationError to avoid spamming errors
   * on simple authentication problem
   */
  useEffect(() => {
    if (error) {
      const { networkError, graphQLErrors } = error;
      let errorHandled;
      let isAuthenticationError;
      if (networkError) {
        networkError.result?.errors?.forEach(({ message, extensions }) => {
          errorHandled = true;
          // Catch expired token exception
          if (extensions?.code === 'UNAUTHENTICATED') {
            isAuthenticationError = true;
            toast.warn(wording.reconnect);
            // trigger login popup
            expireUserToken();
          } else if (!silentError) {
            toast.error(message);
          }
        });
        // send unexpected server errors (ie: code > 200 and not an authentication error) to datadog
        /** should opt out for 498 custom code */
        if (networkError.statusCode > 299 && !isAuthenticationError) {
          logger.error(
            `[request-error] status code ${networkError.statusCode}`,
            { networkError }
          );
        }
        /** the networkError.result?.errors?.forEach loop should be done here */
        // handle the critical "Request header too large" error even if silentError is true
        if (networkError.result?.error?.code === 494) {
          errorHandled = true;
          openCleanModal();
        }
        if (!errorHandled) {
          errorHandled = true;
          if (!silentError) {
            // extract errorMessage in custom NGINX responses if applicable
            toast.error(
              networkError.result?.error?.message || networkError.message
            );
          }
        }
      }
      if (graphQLErrors) {
        graphQLErrors.forEach(({ message, extensions }) => {
          errorHandled = true;
          if (!silentError) {
            const expectedError = expectedErrors?.find(
              (e) => e.code === extensions?.code
            );
            toast.error(expectedError?.message || message);
          }
        });
      }
      // if the error was not handled display a default message
      // this should never happen
      if (!errorHandled) {
        logger.error(`[request-error] request error unhandled`);
        if (!silentError) toast.error(wording.serverError);
      }
    }
  }, [error, silentError, expireUserToken, expectedErrors, openCleanModal]);
};

export default useQueryErrors;
