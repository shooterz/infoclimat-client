import { useLazyQuery } from '@apollo/client';

import useQueryErrors from './useQueryErrors';

const useSafeLazyQuery = (query, queryOptions, errorOptions) => {
  const [cb, { error, ...useQueryResult }] = useLazyQuery(query, queryOptions);
  useQueryErrors(error, errorOptions);

  return [cb, { error, ...useQueryResult }];
};

export default useSafeLazyQuery;
