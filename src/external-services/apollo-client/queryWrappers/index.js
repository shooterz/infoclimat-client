import useSafeLazyQuery from './useSafeLazyQuery';
import useSafeMutation from './useSafeMutation';
import useSafeQuery from './useSafeQuery';

export { useSafeQuery, useSafeLazyQuery, useSafeMutation };
