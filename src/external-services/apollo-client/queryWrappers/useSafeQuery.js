import { useQuery } from '@apollo/client';

import useQueryErrors from './useQueryErrors';

const useSafeQuery = (query, queryOptions, errorOptions) => {
  const { error, ...useQueryResult } = useQuery(query, queryOptions);
  useQueryErrors(error, errorOptions);

  return { error, ...useQueryResult };
};

export default useSafeQuery;
