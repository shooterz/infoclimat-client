import { ApolloClient, ApolloLink, InMemoryCache } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { createUploadLink } from 'apollo-upload-client';
import CONFIG from 'config/index';

const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext({
    uri: CONFIG.apiUrl,
  });

  return forward(operation);
});

// inspired from here https://github.com/apollographql/apollo-feature-requests/issues/153#issuecomment-513148734
const errorLink = onError(({ networkError }) => {
  if (networkError && networkError.bodyText) {
    // Check if error response is JSON
    try {
      JSON.parse(networkError.bodyText);
    } catch (e) {
      // If not replace parsing error message with real one
      // eslint-disable-next-line no-param-reassign
      networkError.message = networkError.bodyText;
    }
  }
});

const uploadLink = createUploadLink();

const link = ApolloLink.from([authMiddleware, errorLink, uploadLink]);

const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link,
});

export default apolloClient;
