import createSvgIcon from '@material-ui/core/utils/createSvgIcon';

const Filters = createSvgIcon(
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 20 20"
    fill="currentColor"
  >
    <path d="M8.55755 9.67045C8.68463 9.80749 8.7544 9.9865 8.7544 10.1717V15.6286C8.7544 15.957 9.15433 16.1236 9.39105 15.8928L10.9272 14.1483C11.1328 13.9038 11.2462 13.7828 11.2462 13.5409V10.1729C11.2462 9.98774 11.3172 9.80872 11.443 9.67167L15.851 4.93211C16.1811 4.57655 15.927 4 15.4386 4H4.56196C4.07358 4 3.81817 4.57532 4.14957 4.93211L8.55755 9.67045Z" />
  </svg>,
  'Filters',
);

export default Filters;
