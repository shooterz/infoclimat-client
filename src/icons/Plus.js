import createSvgIcon from '@material-ui/core/utils/createSvgIcon';

const Plus = createSvgIcon(
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 20 20"
    fill="currentColor"
  >
    <path d="M16.2999 9.29998H10.7V3.69996C10.7 3.31364 10.3864 3 9.99994 3C9.61362 3 9.29998 3.31364 9.29998 3.69996V9.29998H3.69996C3.31364 9.29998 3 9.61362 3 9.99994C3 10.3864 3.31364 10.7 3.69996 10.7H9.29998V16.2999C9.29998 16.6864 9.61362 17 9.99994 17C10.3864 17 10.7 16.6864 10.7 16.2999V10.7H16.2999C16.6864 10.7 17 10.3864 17 9.99994C17 9.61362 16.6864 9.29998 16.2999 9.29998V9.29998Z" />
  </svg>, 'Plus',
);

export default Plus;
