import Layout from 'components/Layout';
import { Suspense, lazy } from 'react';
import LoadingScreen from './components/LoadingScreen';

const Loadable = (Component) => (props) => (
  <Suspense fallback={<LoadingScreen />}>
    <Component {...props} />
  </Suspense>
);

const HomeView = Loadable(lazy(() => import('pages/home/HomeView')));
const EventDetailsView = Loadable(
  lazy(() => import('pages/eventDetails/EventDetailsView')),
);
const NotFound = Loadable(lazy(() => import('pages/errors/NotFound')));
const ServerError = Loadable(lazy(() => import('pages/errors/ServerError')));

const routes = [
  {
    path: '*',
    element: <Layout />,
    children: [
      {
        path: '/',
        element: <HomeView />,
      },
      {
        path: '/events/:eventId',
        element: <EventDetailsView />,
      },
      {
        path: '404',
        element: <NotFound />,
      },
      {
        path: '500',
        element: <ServerError />,
      },

      {
        path: '*',
        element: <NotFound />,
      },
    ],
  },
];

export default routes;
