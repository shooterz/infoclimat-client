import events from './events';
import formatFilters, { defaultFiltersOptions } from './formatFilters';

const useFiltersReducer = (initialState, options = defaultFiltersOptions) => {
  const filterReducer = (state, action) => {
    switch (action.type) {
      case events.CHANGE_FILTER: {
        const { key, value } = action.payload;
        const newState = {
          ...state,
          [key]: value,
        };
        return formatFilters(newState, options);
      }
      case events.CHANGE_FILTERS: {
        const newState = {
          ...state,
          ...action.payload,
        };
        return formatFilters(newState, options);
      }
      case events.RESET_FILTERS:
        return formatFilters(initialState, options);
      default:
        throw new Error(`Action ${action.type} is not defined.`);
    }
  };

  return { initialState, reducer: filterReducer };
};

export default useFiltersReducer;
