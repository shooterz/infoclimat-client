import moment from 'moment';
import { formatInputDateTime } from 'utils/formatDate';

const defaultFiltersOptions = {
  toTimestamp: [],
  toInputDate: [],
  nullIfFalse: [],
};

const formatFilters = (state, filtersOptions = defaultFiltersOptions) => {
  const { toTimestamp, toInputDate, nullIfFalse } = filtersOptions;
  const filters = Object.keys(state).reduce((acc, key) => {
    if (toTimestamp?.includes(key)) acc[key] = moment(state[key]).valueOf();
    if (toInputDate?.includes(key)) acc[key] = formatInputDateTime(state[key]);
    if (nullIfFalse?.includes(key)) acc[key] = state[key] === false ? null : state[key];
    return acc;
  }, { ...state });

  return filters;
};

export { defaultFiltersOptions };
export default formatFilters;
