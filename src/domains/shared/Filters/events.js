const events = {
  CHANGE_FILTER: 'changeFilter',
  CHANGE_FILTERS: 'changeFilters',
  RESET_FILTERS: 'resetFilters',
};

export default events;
