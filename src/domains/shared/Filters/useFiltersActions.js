import events from './events';

const useFiltersActions = (dispatch) => ({
  changeFilter: (key, value) => dispatch({
    type: events.CHANGE_FILTER,
    payload: { key, value },
  }),
  changeFilters: (filters) => dispatch({
    type: events.CHANGE_FILTERS,
    payload: filters,
  }),
  resetFilters: () => dispatch({ type: events.RESET_FILTERS }),
});

export default useFiltersActions;
