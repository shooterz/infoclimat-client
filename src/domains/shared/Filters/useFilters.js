import { useReducer } from 'react';
import useFiltersActions from './useFiltersActions';
import useFiltersReducer from './useFiltersReducer';

const useFilters = (initialState, options) => {
  const { reducer } = useFiltersReducer(initialState, options);
  const [filters, dispatch] = useReducer(reducer, initialState);
  const { changeFilter, changeFilters, resetFilters } = useFiltersActions(dispatch);

  return [filters, { changeFilter, changeFilters, resetFilters }];
};

// eslint-disable-next-line import/prefer-default-export
export { useFilters };
