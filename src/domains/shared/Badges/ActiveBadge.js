import Badge from 'components/Badge';
import React from 'react';

const computeBadgeStatus = ({ active }) => (active ? ({
  color: 'success',
  label: 'actif',
}) : ({
  color: 'error',
  label: 'inactif',
}));

const UserStatusBadge = ({ active }) => {
  const { color, label } = computeBadgeStatus({ active });
  return (
    <Badge color={color}>{label}</Badge>
  );
};

export default UserStatusBadge;
