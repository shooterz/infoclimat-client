import { useState } from 'react';

const usePagination = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(30);

  return {
    page, rowsPerPage, changePage: setPage, rowsPerPageChange: setRowsPerPage,
  };
};

export default usePagination;
