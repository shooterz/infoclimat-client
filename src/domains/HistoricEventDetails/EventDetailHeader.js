import {
  Grid, Card, CardContent, Box, Typography, AvatarGroup, Avatar,
} from '@material-ui/core';
import { getImage, getType } from 'constants/types';
import ucFirst from '../../utils/ucFirst';

const EventDetailHeader = ({ eventDetail }) => (
  <Grid
    container
    spacing={3}
    mb={3}
  >
    <Grid
      item
      xs={12}
    >
      <Card sx={{
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'column',
      }}
      >
        <AvatarGroup max={2} sx={{ mt: 3 }}>
          {getImage(eventDetail.type).map((icon) => <Avatar src={icon} />)}
        </AvatarGroup>
        <CardContent>
          <Grid
            container
            spacing={3}
          />

          <Box sx={{ mt: 3 }}>
            <Typography
              color="textPrimary"
              variant="h4"
            >
              {ucFirst(eventDetail.nom || getType(eventDetail.type))}
            </Typography>
          </Box>
        </CardContent>
      </Card>
    </Grid>
  </Grid>
);

export default EventDetailHeader;
