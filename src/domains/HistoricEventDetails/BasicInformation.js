/* eslint-disable object-curly-newline */
import Markdown from 'react-markdown/with-html';
import {
  Box, Card, CardContent, Chip, Grid, Typography, List, ListItem, ListItemText, Link,
} from '@material-ui/core';
import { experimentalStyled } from '@material-ui/core/styles';
import moment from 'moment';
import { useState } from 'react';
import { getLocalisationsArrayValues, getLocalisationsDepartments } from 'constants/localisations';
import { getImportance } from 'constants/importances';
import France from '@socialgouv/react-departements';
import useScreenType from '../../theme/useScreenType';
import HistoricValuesDialog from './HistoricValuesDialog';

const MarkdownWrapper = experimentalStyled('div')(({ theme }) => ({
  color: theme.palette.text.primary,
  fontFamily: theme.typography.fontFamily,
  '& p': {
    marginBottom: theme.spacing(2),
  },
}));

const BasicInformation = ({ eventDetail }) => {
  const { isMobile, isTablet } = useScreenType();
  const [departmentCode, setDepartmentCode] = useState();
  const {
    shortDesc, dateDeb, dateFin, duree, localisation, importance, hits, why,
  } = eventDetail;

  return (
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
      }}
    >
      <Grid
        container
        spacing={2}
      >
        <Grid
          item
          md={6}
          xs={12}
        >
          <Card>
            <CardContent>
              <Typography
                color="textPrimary"
                variant="subtitle2"

              >
                Statistiques
              </Typography>
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <Box sx={{ mt: 3 }}>
                    <Typography
                      color="textSecondary"
                      variant="overline"
                    >
                      Importance
                    </Typography>
                    <Box sx={{ mt: 1 }}>
                      <Chip
                        key={importance}
                        label={getImportance(importance).label}
                        color="primary"
                        variant="contained"
                      />
                    </Box>
                  </Box>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <Box sx={{ mt: 3 }}>
                    <Typography
                      color="textSecondary"
                      variant="overline"
                    >
                      DÉPARTEMENT(S) IMPACTÉ(S)
                    </Typography>

                    <Box sx={{ mt: 1 }}>
                      <Chip
                        key={importance}
                        label={`${getLocalisationsArrayValues(localisation).length} département(s)`}
                        color="primary"
                        variant="contained"
                      />
                    </Box>
                  </Box>
                </Grid>

                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <Box sx={{ mt: 3 }}>
                    <Typography
                      color="textSecondary"
                      variant="overline"
                    >
                      VUES
                    </Typography>

                    <Box sx={{ mt: 1 }}>
                      <Chip
                        key={importance}
                        label={`${hits} vue(s)`}
                        color="primary"
                        variant="contained"
                      />
                    </Box>
                  </Box>
                </Grid>

              </Grid>
            </CardContent>
          </Card>
        </Grid>
        <Grid
          item
          md={6}
          xs={12}
        >
          <Card>
            <CardContent>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                Dates
              </Typography>
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <Box sx={{ mt: 3 }}>
                    <Typography
                      color="textSecondary"
                      variant="overline"
                    >
                      DURÉE TOTALE
                    </Typography>
                    <Box sx={{ mt: 1 }}>
                      <Chip
                        key={duree}
                        label={`${duree} jour(s)`}
                        color="primary"
                        variant="contained"
                      />
                    </Box>
                  </Box>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <Box sx={{ mt: 3 }}>
                    <Typography
                      color="textSecondary"
                      variant="overline"
                    >
                      DATE DE DÉBUT
                    </Typography>

                    <Box sx={{ mt: 1 }}>
                      <Chip
                        key={importance}
                        label={moment(dateDeb).format('DD-MM-YYYY')}
                        color="primary"
                        variant="contained"
                      />
                    </Box>
                  </Box>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <Box sx={{ mt: 3 }}>
                    <Typography
                      color="textSecondary"
                      variant="overline"
                    >
                      DATE DE FIN
                    </Typography>
                    <Box sx={{ mt: 1 }}>
                      <Chip
                        key={importance}
                        label={moment(dateFin).format('DD-MM-YYYY')}
                        color="primary"
                        variant="contained"
                      />
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
        { (shortDesc || why) && (
        <Grid
          item
          xs={12}
        >
          <Card>
            <CardContent>
              <Typography
                color="textPrimary"
                variant="subtitle2"
                sx={{ mb: 3 }}
              >
                Détails
              </Typography>
              <Grid
                container
                spacing={3}
              />
              { shortDesc && (
                <Box sx={{ mt: 3 }}>
                  <Typography
                    color="textSecondary"
                    sx={{ mb: 2 }}
                    variant="overline"
                  >
                    DESCRIPTION
                  </Typography>
                  <MarkdownWrapper>
                    {/* {description && (
                  <div dangerouslySetInnerHTML={{ __html: description }} />
                  )} */}
                    <Markdown source={shortDesc} />
                  </MarkdownWrapper>
                </Box>
              )}
              { why && (
                <Box sx={{ mt: 3 }}>
                  <Typography
                    color="textSecondary"
                    sx={{ mb: 2 }}
                    variant="overline"
                  >
                    POURQUOI ?
                  </Typography>
                  <MarkdownWrapper>
                    <Markdown source={why} />
                  </MarkdownWrapper>
                </Box>
              )}
            </CardContent>
          </Card>
        </Grid>
        )}
        <Grid
          item
          xs={12}
        >
          <Card>
            <CardContent>
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  xs={12}
                  md={9}
                >
                  <Box>
                    <Typography
                      color="textPrimary"
                      variant="subtitle2"
                      sx={{ mb: 3 }}
                    >
                      DÉPARTEMENTS IMPACTÉS
                    </Typography>

                    <Box sx={{ mt: !isMobile && 1, justifyContent: 'center', display: 'flex', marginTop: isMobile && '-70px' }}>
                      <France departements={getLocalisationsDepartments(localisation)} />
                    </Box>
                  </Box>
                </Grid>
                <Grid
                  item
                  xs={12}
                  md={3}
                >
                  <Box sx={{ mt: !isMobile ? 3 : 0 }}>
                    <Box sx={{ mt: !isMobile ? 1 : 0, justifyContent: 'center', display: 'flex' }}>
                      <List sx={{
                        maxHeight: isMobile ? '200px' : '600px', width: isMobile ? '100%' : 'auto', marginTop: isMobile && '-80px', overflow: 'auto',
                      }}
                      >
                        {getLocalisationsArrayValues(localisation).map((loc) => (
                          <ListItem
                            disableGutters
                            divider
                          >
                            <ListItemText
                              disableTypography
                              primary={(
                                <Typography
                                  color="textPrimary"
                                  variant="subtitle2"
                                >
                                  <Link
                                    color="textPrimary"
                                    variant="subtitle2"
                                    onClick={() => setDepartmentCode(loc.key)}
                                  >
                                    {loc.name}
                                  </Link>
                                </Typography>
                                )}
                            />
                          </ListItem>
                        ))}
                      </List>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      {Boolean(departmentCode) && (
        <HistoricValuesDialog
          onClose={() => setDepartmentCode(null)}
          open={Boolean(departmentCode)}
          departmentCode={departmentCode}
          idHistoric={eventDetail.id}
        />
      )}
    </Box>
  );
};

export default BasicInformation;
