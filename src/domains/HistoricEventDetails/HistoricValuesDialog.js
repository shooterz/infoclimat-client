import {
  Box, Dialog, Typography, Card, CardHeader,
} from '@material-ui/core';
import Chart from 'react-apexcharts';
import { useQuery } from '@apollo/client';
import React from 'react';
import { useTheme } from '@emotion/react';
import { getValueType } from 'constants/valuesType';
import { formatDisplayDate } from 'utils/formatDate';
import { GET_HISTORIC_VALUES } from '../HistoricEvents/List/queries';

const HistoricValuesDialog = ({
  open,
  onClose,
  idHistoric,
  departmentCode,
}) => {
  console.log(departmentCode);
  const { data, loading } = useQuery(GET_HISTORIC_VALUES, {
    variables: { idHistoric: Number(idHistoric), departmentCode: Number(departmentCode) },
  });

  const theme = useTheme();

  const getHistoricValuesByDepartment = data?.getHistoricValuesByDepartment;

  if (!getHistoricValuesByDepartment || loading) return null;

  const datasByTypes = getHistoricValuesByDepartment?.reduce((acc, it) => {
    acc[it.type] = [...(acc[it.type] || []), it];
    return acc;
  }, {});

  const cleanDatas = Object.keys(datasByTypes)?.map((type) => {
    const datasByType = datasByTypes[type];
    return {
      type,
      data: datasByType.reduce((acc, it) => {
        acc[it.geoid] = [...acc[it.geoid] || [], it];
        return acc;
      }, {}),
    };
  }, {});

  const datas = cleanDatas.map((d) => ({
    type: d.type,
    series: Object.keys(d.data).map((d2) => ({
      data: d.data[d2].map((d3) => d3.valeur),
      name: d2,
    })),
    xaxis: {
      dataPoints: Object.keys(d.data).map((d2) => d.data[d2].map((d3) => formatDisplayDate(d3.date))),
    },
  }));

  const chart = {
    options: {
      chart: {
        background: 'transparent',
        stacked: false,
        toolbar: {
          show: false,
        },
        zoom: false,
      },
      // colors: visibleSeries.map((item) => item.color),
      dataLabels: {
        enabled: false,
      },
      grid: {
        borderColor: theme.palette.divider,
        xaxis: {
          lines: {
            show: true,
          },
        },
        yaxis: {
          lines: {
            show: true,
          },
        },
      },
      legend: {
        show: false,
      },
      markers: {
        hover: {
          size: undefined,
          sizeOffset: 2,
        },
        radius: 2,
        shape: 'circle',
        size: 4,
        strokeWidth: 0,
      },
      stroke: {
        curve: 'smooth',
        lineCap: 'butt',
        width: 3,
      },
      theme: {
        mode: theme.palette.mode,
      },
      tooltip: {
        theme: theme.palette.mode,
      },
      xaxis: {
        axisBorder: {
          color: theme.palette.divider,
        },
        axisTicks: {
          color: theme.palette.divider,
          show: true,
        },
        // categories: datas[0].xaxis.dataPoints,
        labels: {
          style: {
            colors: theme.palette.text.secondary,
          },
        },
      },
      yaxis: [
        {
          axisBorder: {
            color: theme.palette.divider,
            show: true,
          },
          axisTicks: {
            color: theme.palette.divider,
            show: true,
          },
          labels: {
            style: {
              colors: theme.palette.text.secondary,
            },
          },
        },
        {
          axisTicks: {
            color: theme.palette.divider,
            show: true,
          },
          axisBorder: {
            color: theme.palette.divider,
            show: true,
          },
          labels: {
            style: {
              colors: theme.palette.text.secondary,
            },
          },
          opposite: true,
        },
      ],
    },
    // series: datas,
  };

  return (
    <Dialog fullWidth open={open} onClose={onClose}>
      <Card sx={{ overflowY: 'scroll' }}>
        <CardHeader
          disableTypography
          title={(
            <Box
              sx={{
                alignItems: 'center',
                display: 'flex',
                justifyContent: 'space-between',
              }}
            >
              <Typography
                color="textPrimary"
                variant="h6"
              >
                Valeurs
              </Typography>
            </Box>
        )}
        />
        {datas.map((d) => (
          <Box sx={{ p: 2 }}>
            <Typography>
              {`${getValueType(d.type).name} (${getValueType(d.type).unit})`}
            </Typography>
            <Chart
              height="393"
              type="line"
              {...{ ...chart, series: d.series, options: { ...chart.options, xaxis: { ...chart.xaxis, categories: d.xaxis.dataPoints[0] } } }}
            />
          </Box>
        ))}
      </Card>
    </Dialog>
  );
};

export default HistoricValuesDialog;
