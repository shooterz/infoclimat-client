import Chart from 'react-apexcharts';
import {
  Avatar, Box, Card, Divider, Grid, Typography,
} from '@material-ui/core';
import { alpha, useTheme } from '@material-ui/core/styles';
import { getHValue } from 'constants/valuesType';
import ChevronDownIcon from '../../icons/ChevronDown';
import ChevronUpIcon from '../../icons/ChevronUp';
import Plus from '../../icons/Plus';
import Minus from '../../icons/Minus';

const LineChart = () => {
  const theme = useTheme();

  const chartOptions = {
    chart: {
      background: 'transparent',
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    colors: ['#7783DB'],
    dataLabels: {
      enabled: false,
    },
    grid: {
      show: false,
    },
    stroke: {
      width: 3,
    },
    theme: {
      mode: theme.palette.mode,
    },
    tooltip: {
      enabled: false,
    },
    xaxis: {
      labels: {
        show: false,
      },
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
    },
    yaxis: {
      show: false,
    },
  };

  const chartSeries = [{ data: [0, 60, 30, 60, 0, 30, 10, 30, 0] }];

  return (
    <Chart
      options={chartOptions}
      series={chartSeries}
      type="line"
      width={100}
    />
  );
};

const BarChart = () => {
  const theme = useTheme();

  const chartOptions = {
    chart: {
      background: 'transparent',
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    colors: ['#7783DB'],
    dataLabels: {
      enabled: false,
    },
    grid: {
      show: false,
    },
    states: {
      normal: {
        filter: {
          type: 'none',
          value: 0,
        },
      },
    },
    stroke: {
      width: 0,
    },
    theme: {
      mode: theme.palette.mode,
    },
    tooltip: {
      enabled: false,
    },
    xaxis: {
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      labels: {
        show: false,
      },
    },
    yaxis: {
      show: false,
    },
  };

  const chartSeries = [{ data: [10, 20, 30, 40, 50, 60, 5] }];

  return (
    <Chart
      options={chartOptions}
      series={chartSeries}
      type="bar"
      width={100}
    />
  );
};

const KPI = ({ eventDetail }) => {
  const currentHValueUnit = getHValue(eventDetail).unit;
  const currentHValueName = getHValue(eventDetail).name;
  return (
    <Grid
      container
      spacing={2}
      mb={3}
    >
      <Grid
        item
        md={3}
        sm={6}
        xs={12}
      >
        <Card>
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'space-between',
              p: 3,
            }}
          >
            <div>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                MAXIMUM
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mt: 1 }}
                variant="h5"
              >
                {`${eventDetail.historicMaxValues} ${currentHValueUnit}`}
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mt: 1 }}
                variant="subtitle2"
              >
                {currentHValueName}
              </Typography>
            </div>
            <LineChart />
          </Box>
          <Divider />
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              px: 3,
              py: 2,
            }}
          >
            <Avatar
              sx={{
                backgroundColor: (theme) => alpha(theme.palette.success.main, 0.08),
                color: 'success.main',
                height: 36,
                width: 36,
              }}
            >
              <Plus fontSize="small" />
            </Avatar>
            <Typography
              color="textSecondary"
              sx={{ ml: 1 }}
              variant="caption"
            >
              10% au dessus des normales de saisons
            </Typography>
          </Box>
        </Card>
      </Grid>
      <Grid
        item
        md={3}
        sm={6}
        xs={12}
      >
        <Card>
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'space-between',
              p: 3,
            }}
          >
            <div>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                MINIMUM
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mt: 1 }}
                variant="h5"
              >
                {`${eventDetail.historicMinValues} ${currentHValueUnit}`}
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mt: 1 }}
                variant="subtitle2"
              >
                {currentHValueName}
              </Typography>
            </div>
            <LineChart />
          </Box>
          <Divider />
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              px: 3,
              py: 2,
            }}
          >
            <Avatar
              sx={{
                backgroundColor: (theme) => alpha(theme.palette.error.main, 0.08),
                color: 'error.main',
                height: 36,
                width: 36,
              }}
            >
              <Minus fontSize="small" />
            </Avatar>
            <Typography
              color="textSecondary"
              sx={{ ml: 1 }}
              variant="caption"
            >
              10% au dessus des normales de saisons
            </Typography>
          </Box>
        </Card>
      </Grid>
      <Grid
        item
        md={3}
        sm={6}
        xs={12}
      >
        <Card>
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'space-between',
              p: 3,
            }}
          >
            <div>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                MOYENNE
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mt: 1 }}
                variant="h5"
              >
                {`${Math.round(eventDetail.historicAvgValues * 100) / 100} ${currentHValueUnit}`}
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mt: 1 }}
                variant="subtitle2"
              >
                {currentHValueName}
              </Typography>
            </div>
            <LineChart />
          </Box>
          <Divider />
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              px: 3,
              py: 2,
            }}
          >
            <Avatar
              sx={{
                backgroundColor: (theme) => alpha(theme.palette.error.main, 0.08),
                color: 'error.main',
                height: 36,
                width: 36,
              }}
            >
              <ChevronDownIcon fontSize="small" />
            </Avatar>
            <Typography
              color="textSecondary"
              sx={{ ml: 1 }}
              variant="caption"
            >
              30% moins que le mois précédent
            </Typography>
          </Box>
        </Card>
      </Grid>
      <Grid
        item
        md={3}
        sm={6}
        xs={12}
      >
        <Card>
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'space-between',
              p: 3,
            }}
          >
            <div>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                ECART
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mt: 1 }}
                variant="h5"
              >
                {`${eventDetail.historicMaxValues - eventDetail.historicMinValues} ${currentHValueUnit}`}
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mt: 1 }}
                variant="subtitle2"
              >
                {currentHValueName}
              </Typography>
            </div>
            <BarChart />
          </Box>
          <Divider />
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              px: 3,
              py: 2,
            }}
          >
            <Avatar
              sx={{
                backgroundColor: (theme) => alpha(theme.palette.success.main, 0.08),
                color: 'success.main',
                height: 36,
                width: 36,
              }}
            >
              <ChevronUpIcon fontSize="small" />
            </Avatar>
            <Typography
              color="textSecondary"
              sx={{ ml: 1 }}
              variant="caption"
            >
              12% de plus que le mois précédent
            </Typography>
          </Box>
        </Card>
      </Grid>
    </Grid>
  );
};

export default KPI;
