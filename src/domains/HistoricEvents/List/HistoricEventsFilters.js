import { Grid } from '@material-ui/core';
import TableFilters from 'components/table/TableFilters';
import React from 'react';
import { useForm } from 'react-hook-form';
import Select from 'components/form/Select';
import Input from 'components/form/Input';
import types from 'constants/types';
import importances from 'constants/importances';
import localisations from 'constants/localisations';
import { MODES, useMode } from 'contexts/ModeContext';

const HistoricEventsFilters = ({ onFiltersChange }) => {
  const { control, handleSubmit, reset } = useForm();
  const { mode } = useMode();

  return (
    <TableFilters
      title="Filtres"
      onSubmit={handleSubmit(async (values) => onFiltersChange(values))}
      onReset={() => reset()}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} md={4}>
          <Select
            label="Type"
            name="type"
            control={control}
            options={Object.keys(types).map((t) => ({
              value: t,
              label: types[t].name,
            }))}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <Select
            label="Localisation"
            name="localisation"
            control={control}
            options={Object.keys(localisations).map((l) => ({
              value: l,
              label: localisations[l],
            }))}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          {mode === MODES.PRO ? (
            <Select
              label="Importance"
              name="importance"
              control={control}
              options={Object.keys(importances).map((i) => ({
                value: Number(i),
                label: importances[i].label,
              }))}
            />
          ) : (
            <Select
              label="Importance"
              name="importance"
              control={control}
              options={[
                {
                  value: 1,
                  label: 'Faible',
                },
                {
                  value: 3,
                  label: 'Moyen',
                },
                {
                  value: 5,
                  label: 'Fort',
                },
              ]}
            />
          )}
        </Grid>
        {mode === MODES.NOVICE && (
        <Grid item xs={12} md={4}>
          <Input
            label="Année"
            name="annee"
            control={control}
          />
        </Grid>
        )}
        {mode === MODES.PRO && (
        <>
          <Grid item xs={12} md={4}>
            <Input
              label="Date de début"
              name="dateDeb"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
              control={control}
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <Input
              label="Date de fin"
              name="dateFin"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
              control={control}
            />
          </Grid>
        </>
        )}
      </Grid>
    </TableFilters>
  );
};

export default HistoricEventsFilters;
