import { Link as RouterLink } from 'react-router-dom';
import {
  Avatar,
  AvatarGroup,
  Box,
  Card,
  Divider,
  experimentalStyled,
  Grid,
  IconButton,
  Link,
  Typography,
} from '@material-ui/core';
import { getImportance } from 'constants/importances';
import { getImage, getType } from 'constants/types';
import EyeIcon from 'icons/Eye';
import moment from 'moment';
import { getLocalisationsCode } from 'constants/localisations';
import {
  TwitterShareButton, TwitterIcon, FacebookShareButton, FacebookIcon,
} from 'react-share';

const TruncatedText = experimentalStyled('div')({
  textOverflow: 'ellipsis',
  maxWidth: '100px',
});

const HistoricEventCard = ({ historicEvent }) => (
  <Card
    sx={{
      mb: 2,
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    }}
  >
    <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
      <Box sx={{ flex: 1 }}>
        <Box sx={{ p: 3 }}>
          <Box
            sx={{
              display: 'flex',
            }}
          >
            <AvatarGroup max={2}>
              {getImage(historicEvent.type).map((icon) => (
                <Avatar src={icon} />
              ))}
            </AvatarGroup>
            <Box sx={{ ml: 2 }}>
              <Link
                color="textPrimary"
                component={RouterLink}
                to={`events/${historicEvent.id}`}
                variant="h6"
              >
                {historicEvent.nom
                                    || getType(historicEvent.type)}
              </Link>
              <Typography color="textSecondary" variant="body2">
                {getImportance(historicEvent.importance)?.label}
              </Typography>
            </Box>
          </Box>
        </Box>
        <Box
          sx={{
            pb: 2,
            px: 3,
          }}
        >
          <Typography color="textSecondary" variant="body2">
            {historicEvent.shortDesc || 'Pas de description'}
          </Typography>
        </Box>
      </Box>
      <Box
        sx={{
          px: 3,
          py: 2,
        }}
      >
        <Grid
          alignItems="center"
          container
          justifyContent="space-between"
          spacing={3}
        >
          <Grid item>
            <Typography color="textPrimary" variant="subtitle2">
              {moment(historicEvent.dateDeb).format('DD/MM/YYYY')}
            </Typography>
            <Typography color="textSecondary" variant="body2">
              Date
            </Typography>
          </Grid>
          <Grid item>
            <TruncatedText>
              <Typography
                noWrap
                color="textPrimary"
                variant="subtitle2"
              >
                {getLocalisationsCode(
                  historicEvent.localisation,
                )}
              </Typography>
            </TruncatedText>
            <Typography color="textSecondary" variant="body2">
              Départements
            </Typography>
          </Grid>
          <Grid item>
            <Typography color="textPrimary" variant="subtitle2">
              {historicEvent.duree}
              {' '}
              jours
            </Typography>
            <Typography color="textSecondary" variant="body2">
              Durée
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Box>
    <Divider />
    <Box
      sx={{
        flex: 0,
        alignItems: 'center',
        display: 'flex',
        pl: 2,
        pr: 3,
        py: 2,
      }}
    >
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          ml: 2,
        }}
      >
        <EyeIcon fontSize="small" />
        <Typography
          color="textSecondary"
          sx={{ ml: 1 }}
          variant="subtitle2"
        >
          {historicEvent.hits}
        </Typography>
      </Box>
      <Box sx={{ flexGrow: 1 }} />
      <IconButton>
        <TwitterShareButton url={`https://info-climat.ilsduc.fr/events/${historicEvent.id}`} title={historicEvent.nom}>
          <TwitterIcon size={32} round />
        </TwitterShareButton>
      </IconButton>
      <IconButton>
        <FacebookShareButton url={`https://info-climat.ilsduc.fr/events/${historicEvent.id}`} quote={historicEvent.nom}>
          <FacebookIcon size={32} round />
        </FacebookShareButton>
      </IconButton>
    </Box>
  </Card>
);

export default HistoricEventCard;
