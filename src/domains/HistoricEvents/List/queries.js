import { gql } from '@apollo/client';

const GET_HISTORIC_EVENTS = gql`
    query getHistoricEvents($cursor: Int!, $limit: Int!, $filters: HistoricEventFiltersInput!, $orderBy: Ordering) {
        getHistoricEvents(cursor: $cursor, limit: $limit, filters: $filters orderBy: $orderBy) {
            total
            historicEvents {
                id
                nom
                localisation
                importance
                typeCyclone
                hasImageCyclone
                dateDeb
                dateFin
                duree
                type
                description
                shortDesc
                sources
                idCompte
                valeurMax
                bsLink
                genCartes
                why
                hits
                notes
                historicCountValues
            }
        }
    }
`;
const GET_HISTORIC_VALUES = gql`
    query getHistoricValuesByDepartment (
        $idHistoric: Int!
        $departmentCode: Int!) {
    getHistoricValuesByDepartment (idHistoric: $idHistoric, departmentCode: $departmentCode) {
        id
        lieu
        dept
        valeur
        date
        type
        geoid
    }
}
`;

export { GET_HISTORIC_EVENTS, GET_HISTORIC_VALUES };
