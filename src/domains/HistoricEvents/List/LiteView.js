import React from 'react';
import { Box, Grid, Pagination } from '@material-ui/core';
import HistoricEventsFilters from './HistoricEventsFilters';

import HistoricEventCard from './HistoricEventCard';

const LiteView = ({
  historicEvents, total, rowsPerPage, onChangePage, onFiltersChange,
}) => (
  <Box sx={{ mt: 2 }}>
    <HistoricEventsFilters onFiltersChange={onFiltersChange} />
    <Box sx={{ mt: 2 }}>
      <Grid container spacing={2}>
        {historicEvents.map((historicEvent) => (
          <Grid item xs={12}>
            <HistoricEventCard historicEvent={historicEvent} />
          </Grid>
        ))}
      </Grid>
    </Box>
    <Box sx={{ my: 2, display: 'flex', justifyContent: 'center' }}>
      <Pagination
        count={Math.ceil(total / rowsPerPage)}
        onChange={(e, p) => onChangePage(p - 1)}
      />
    </Box>
  </Box>
);

export default LiteView;
