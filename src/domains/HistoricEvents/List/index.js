import React, { useEffect } from 'react';
import { useLazyQuery } from '@apollo/client';
import LoadingScreen from 'components/LoadingScreen';
import usePagination from 'domains/shared/Pagination/usePagination';
import { useFilters } from 'domains/shared/Filters/useFilters';
import { MODES, useMode } from 'contexts/ModeContext';
import { Container } from '@material-ui/core';
import { GET_HISTORIC_EVENTS } from './queries';
import LiteView from './LiteView';
import ProView from './ProView';

const initialFilters = {
  type: '',
  duree: '',
  localisation: '',
  importance: null,
  dateDeb: '',
  dateFin: '',
};

const HistoriEventsList = () => {
  const { page: cursor, rowsPerPage, changePage } = usePagination();
  const [getHistoricEvents, { data, loading, error }] = useLazyQuery(GET_HISTORIC_EVENTS, {
    fetchPolicy: 'network-only',
  });
  const [filters, { changeFilters }] = useFilters(initialFilters);
  const { mode } = useMode();

  useEffect(() => {
    getHistoricEvents({
      variables: {
        cursor,
        limit: rowsPerPage,
        filters,
      },
    });
  }, [cursor, rowsPerPage, filters]);

  const historicEvents = data?.getHistoricEvents?.historicEvents || [];
  const hEventsCount = data?.getHistoricEvents?.total;

  if (error) return 'An error occurred';
  if (!historicEvents && loading) return <LoadingScreen />;

  if (mode === MODES.NOVICE) {
    return (
      <Container maxWidth="sm">
        <LiteView
          historicEvents={historicEvents}
          total={hEventsCount}
          onChangePage={changePage}
          rowsPerPage={rowsPerPage}
          onFiltersChange={changeFilters}
        />
      </Container>
    );
  }

  return (
    <ProView
      historicEvents={historicEvents}
      total={hEventsCount}
      onChangePage={changePage}
      rowsPerPage={rowsPerPage}
      onFiltersChange={changeFilters}
      page={cursor}
      loading={loading}
    />
  );
};

export default HistoriEventsList;
