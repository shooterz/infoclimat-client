import {
  TableCell,
  TableRow,
  Pagination,
  Box,
  AvatarGroup,
  Avatar,
} from '@material-ui/core';
import ScrollableTable from 'components/table/ScrollableTable';
import React from 'react';
import { formatDisplayDate } from 'utils/formatDate';
import TableLayout from 'components/table/TableLayout';
import { getImage, getType } from 'constants/types';
import { getLocalisations } from 'constants/localisations';
import { getImportance } from 'constants/importances';
import { getDuree } from 'constants/durees';
import { useNavigate } from 'react-router-dom';
import HistoricEventsFilters from './HistoricEventsFilters';

const ProView = ({
  historicEvents, total, rowsPerPage, onChangePage, onFiltersChange, loading,
}) => {
  const TableHeadComponent = (
    <TableRow>
      <TableCell />
      <TableCell>Nom</TableCell>
      <TableCell>Types</TableCell>
      <TableCell>Date de début</TableCell>
      <TableCell>Date de fin</TableCell>
      <TableCell>Durée</TableCell>
      <TableCell>Lieux</TableCell>
      <TableCell>Importance</TableCell>
      <TableCell>Description</TableCell>
      <TableCell>
        Valeurs de l&apos;évènement
      </TableCell>
    </TableRow>
  );

  const TableBodyComponent = historicEvents.map((hEvent) => {
    const navigate = useNavigate();
    return (
      <TableRow
        hover
        key={hEvent.id}
        onClick={() => navigate(`events/${hEvent.id}`)}
        sx={{ cursor: 'pointer' }}
      >
        <TableCell>
          <AvatarGroup max={2}>
            {getImage(hEvent.type).map((icon) => <Avatar src={icon} />)}
          </AvatarGroup>
        </TableCell>
        <TableCell>{hEvent.nom || getType(hEvent.type)}</TableCell>
        <TableCell>{getType(hEvent.type)}</TableCell>
        <TableCell>{formatDisplayDate(hEvent.dateDeb)}</TableCell>
        <TableCell>{formatDisplayDate(hEvent.dateFin)}</TableCell>
        <TableCell>
          {getDuree(hEvent.duree)}
          {' '}
          jours
        </TableCell>
        <TableCell>{getLocalisations(hEvent.localisation)}</TableCell>
        <TableCell>{getImportance(hEvent.importance).label}</TableCell>
        <TableCell>
          {hEvent.shortDesc.length > 40 ? `${hEvent.shortDesc.substr(0, 40)} ...` : hEvent.shortDesc}
        </TableCell>
        <TableCell>
          {hEvent.historicCountValues > 0 ? `${hEvent.historicCountValues} valeur(s)` : 'Pas de valeur'}
        </TableCell>
      </TableRow>
    );
  });

  const TablePaginationComponent = (
    <Box sx={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
      <Pagination
        count={Math.ceil(total / rowsPerPage)}
        onChange={(e, p) => onChangePage(p - 1)}
      />
    </Box>
  );

  const TableComponent = (
    <ScrollableTable
      TableHeadComponent={TableHeadComponent}
      TableBodyComponent={TableBodyComponent}
      TablePaginationComponent={TablePaginationComponent}
      showActions={false}
      loading={loading}
    />
  );

  return (
    <TableLayout
      FiltersComponent={(
        <HistoricEventsFilters onFiltersChange={onFiltersChange} />
        )}
      TableComponent={TableComponent}
    />
  );
};

export default ProView;
