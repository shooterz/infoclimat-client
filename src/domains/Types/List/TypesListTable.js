import {
  TableCell,
  TableRow,
  Checkbox,
} from '@material-ui/core';
import Pagination from 'components/table/Pagination';
import ScrollableTable from 'components/table/ScrollableTable';
import useRowSelection from 'components/table/useRowSelection';
import TableActions from 'components/table/TableActions';
import React from 'react';
import TableActionButton from 'components/table/TableActionButton';
import { formatDisplayDateTime } from 'utils/formatDate';
import ActiveBadge from 'domains/shared/Badges/ActiveBadge';
import LinkCell from 'components/table/cells/LinkCell';
import TrashIcon from 'icons/Trash';
import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { DELETE_TYPE } from './mutations';

const TypesListTable = ({
  types, typesCount, loading, page, rowsPerPage, onPageChange,
}) => {
  const {
    onSelectRow, isSelected, selected, unselectAll,
  } = useRowSelection();
  const { enqueueSnackbar } = useSnackbar();
  const [deleteType] = useMutation(DELETE_TYPE, {
    refetchQueries: ['listTypes'],
    onCompleted: () => enqueueSnackbar('Le type a été supprimé', { variant: 'success' }),
  });

  const TableHeadComponent = (
    <TableRow>
      <TableCell />
      <TableCell>Label</TableCell>
      <TableCell>URL</TableCell>
      <TableCell align="center">Statut</TableCell>
      <TableCell>Date de création</TableCell>
    </TableRow>
  );

  const TableBodyComponent = types.map((type) => (
    <TableRow
      hover
      key={type.id}
    >
      <TableCell
        onClick={() => onSelectRow(type.id)}
        padding="checkbox"
      >
        <Checkbox
          checked={isSelected(type.id)}
          color="primary"
        />
      </TableCell>
      <LinkCell to={`/admin/types/${type.id}/edit`}>{type.label}</LinkCell>
      <TableCell>{type.value}</TableCell>
      <TableCell align="center"><ActiveBadge active={type.active} /></TableCell>
      <TableCell>{formatDisplayDateTime(type.createdAt)}</TableCell>
    </TableRow>
  ));

  const TablePaginationComponent = (
    <Pagination
      onPageChange={onPageChange}
      count={typesCount}
      rowsPerPage={rowsPerPage}
      page={page}
    />
  );

  const TableActionsComponent = (
    <TableActions selectedRows={selected.length}>
      <TableActionButton
        onClick={async () => {
          await Promise.all(selected.map(async (id) => deleteType({ variables: { id } })));
          unselectAll();
        }}
        color="error"
        startIcon={<TrashIcon />}
      >
        Supprimer
      </TableActionButton>
    </TableActions>
  );

  return (
    <ScrollableTable
      TableHeadComponent={TableHeadComponent}
      TableBodyComponent={TableBodyComponent}
      TablePaginationComponent={TablePaginationComponent}
      TableActionsComponent={TableActionsComponent}
      showActions={selected.length > 0}
      loading={loading}
    />
  );
};

export default TypesListTable;
