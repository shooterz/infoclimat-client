import React, { useEffect } from 'react';
import TableLayout from 'components/table/TableLayout';
import { useLazyQuery } from '@apollo/client';
import LoadingScreen from 'components/LoadingScreen';
import usePagination from 'domains/shared/Pagination/usePagination';
import { useFilters } from 'domains/shared/Filters/useFilters';
import { useNavigate } from 'react-router';
import TypesListTable from './TypesListTable';
import { GET_TYPES } from './queries';
import ActionBar from './ActionBar';

const initialFilters = {
  search: null,
};

const TypesList = () => {
  const { page, rowsPerPage, changePage } = usePagination();
  const [getTypes, { data, loading, error }] = useLazyQuery(GET_TYPES, {
    fetchPolicy: 'no-cache',
  });
  const [{ search }, { changeFilters }] = useFilters(initialFilters);
  const navigate = useNavigate();

  const onNewType = () => navigate('/admin/types/new');

  useEffect(() => {
    getTypes({
      variables: {
        page,
        limit: rowsPerPage,
        search,
      },
    });
  }, [page, rowsPerPage, search]);

  const types = data?.listTypes?.types || [];
  const typesCount = data?.listTypes?.total;

  if (error) return 'An error occurred';
  if (!types) return <LoadingScreen />;

  const TableComponent = (
    <TypesListTable
      types={types}
      typesCount={typesCount}
      onPageChange={changePage}
      page={page}
      rowsPerPage={rowsPerPage}
      loading={loading}
    />
  );

  return (
    <TableLayout
      FiltersComponent={(
        <ActionBar
          search={search}
          onSearch={(s) => {
            changeFilters({
              search: s,
              page: 0,
            });
          }}
          onNewType={onNewType}
        />
        )}
      TableComponent={TableComponent}
    />
  );
};

export default TypesList;
