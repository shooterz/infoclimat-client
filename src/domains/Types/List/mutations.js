import { gql } from '@apollo/client';

const DELETE_TYPE = gql`
    mutation deleteType($id: ID!) {
        deleteType(id: $id)
    }
`;

// eslint-disable-next-line import/prefer-default-export
export { DELETE_TYPE };
