import {
  Card, IconButton, InputBase, Toolbar,
} from '@material-ui/core';
import PlusIcon from 'icons/Plus';
import CloseIcon from 'icons/Close';
import SearchIcon from 'icons/Search';
import React from 'react';

const ActionBar = ({ onSearch, search, onNewType }) => (
  <Card>
    <Toolbar disableGutters variant="dense" sx={{ justifyContent: 'space-between' }}>
      <InputBase
        autoFocus
        fullWidth
        onChange={(e) => onSearch(e.target.value)}
        value={search}
        placeholder="Rechercher"
        startAdornment={(
          <IconButton onClick={() => search && onSearch('')}>
            {search ? <CloseIcon /> : <SearchIcon />}
          </IconButton>
        )}
      />
      <IconButton onClick={onNewType}>
        <PlusIcon />
      </IconButton>
    </Toolbar>
  </Card>
);

export default ActionBar;
