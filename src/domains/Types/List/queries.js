import { gql } from '@apollo/client';

const GET_TYPES = gql`
    query listTypes($page: Int, $limit: Int, $search: String) {
        listTypes(page: $page, limit: $limit, search: $search) {
            total
            types {
                id
                slug
                label
                seoTitle
                seoDescription
                active
                createdAt
            }
        }
    }
`;

// eslint-disable-next-line import/prefer-default-export
export { GET_TYPES };
