import { useMutation } from '@apollo/client';
import {
  Box, Card, Grid, Typography,
} from '@material-ui/core';
import Input from 'components/form/Input';
import Checkbox from 'components/form/Checkbox';
import FormLayout from 'components/form/layout/FormLayout';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router';
import { useSnackbar } from 'notistack';
import slugify from 'slugify';
import SEOPlaceholder from 'components/SEOPlaceholder';
import { CREATE_TYPE, UPDATE_TYPE } from './mutations';

const TypeEditorForm = ({ initialType }) => {
  const creating = !initialType;
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const [slug, setSlug] = useState(initialType?.slug || '');

  const {
    control, handleSubmit, watch,
  } = useForm();

  const currentLabel = watch('label', initialType?.label || '');
  const currentSeoTitle = watch('seoTitle', initialType?.seoTitle);
  const currentSeoDescription = watch('seoDescription', initialType?.seoDescription);

  const [saveType] = useMutation(creating ? CREATE_TYPE : UPDATE_TYPE, {
    refetchQueries: ['listTypes'],
    onCompleted: () => navigate(-1),
    onError: () => enqueueSnackbar("Error lors de l'ajout du type", { variant: 'error' }),
  });

  useEffect(() => {
    setSlug(slugify(currentLabel).toLowerCase());
  }, [currentLabel]);

  const title = creating ? 'Ajouter un type' : `${initialType.label}`;

  return (
    <FormLayout
      title={title}
      onSubmit={handleSubmit(async (input) => saveType({
        variables: {
          id: creating ? undefined : initialType.id,
          input: { ...input, slug },
        },
      }))}
    >
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ mb: 1 }} variant="h6" color="textPrimary">
          Général
        </Typography>
        <Card sx={{ p: 2 }}>
          {!creating && (
            <Grid sx={{ mb: 2 }} container spacing={2}>
              <Grid item xs={12} sm={6} md={3}>
                <Checkbox
                  defaultChecked={initialType?.active}
                  label="Active"
                  control={control}
                  name="active"
                />
              </Grid>
            </Grid>
          )}
          <Grid sx={{ mb: 2 }} container spacing={2}>
            <Grid item xs={12} sm={6} md={3}>
              <Input
                defaultValue={initialType?.slug}
                label="Slug"
                value={`/${slug}`}
                disabled
                control={control}
                name="slug"
              />
            </Grid>
          </Grid>
          <Grid sx={{ mb: 2 }} container spacing={2}>
            <Grid item xs={12} sm={6} md={3}>
              <Input
                defaultValue={initialType?.label}
                required
                label="Label"
                control={control}
                name="label"
              />
            </Grid>
          </Grid>
        </Card>
      </Box>
      <Box sx={{ mb: 3 }}>
        <Typography sx={{ mb: 1 }} variant="h6" color="textPrimary">
          SEO de la page
        </Typography>
        <Card sx={{ p: 2 }}>
          <Grid sx={{ mb: 2 }} container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Input
                defaultValue={initialType?.seoTitle}
                required
                label="Titre SEO"
                control={control}
                name="seoTitle"
                helperText="Il est conseillé d'écrire chaque mot avec une majuscule. Cela augmente le taux de clic."
              />
            </Grid>
          </Grid>
          <Grid sx={{ mb: 2 }} container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Input
                defaultValue={initialType?.seoDescription}
                multiline
                rows={5}
                label="Description SEO"
                control={control}
                name="seoDescription"
                helperText="Seul les 155 premiers caractères seront affichés par google. Laissez vide pour laissez les moteurs de recherche gérer la description."
              />
            </Grid>
          </Grid>
          <Grid sx={{ mb: 2 }} container spacing={2}>
            <Grid item xs={12} sm={6}>
              <SEOPlaceholder
                title={currentSeoTitle}
                description={currentSeoDescription}
              />
            </Grid>
          </Grid>
        </Card>
      </Box>
    </FormLayout>
  );
};

export default TypeEditorForm;
