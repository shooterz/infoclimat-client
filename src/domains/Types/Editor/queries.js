import { gql } from '@apollo/client';

const GET_TYPE = gql`
    query getType($id: ID!) {
        getType(id: $id) {
            id
            slug
            label
            seoTitle
            seoDescription
            active
        }
    }
`;

// eslint-disable-next-line import/prefer-default-export
export { GET_TYPE };
