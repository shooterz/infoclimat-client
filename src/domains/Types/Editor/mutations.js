import { gql } from '@apollo/client';

const CREATE_TYPE = gql`
    mutation createType($input: TypeCreateInput!) {
        createType(input: $input) {
            slug
            label
            seoTitle
            seoDescription
            active
            createdAt
        }
    }
`;

const UPDATE_TYPE = gql`
    mutation updateType($id: ID!, $input: TypeUpdateInput!) {
        updateType(id: $id, input: $input) {
            slug
            label
            seoTitle
            seoDescription
            active
            createdAt
        }
    }
`;

export { CREATE_TYPE, UPDATE_TYPE };
