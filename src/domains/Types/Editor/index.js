import { useQuery } from '@apollo/client';
import NotFound from 'pages/errors/NotFound';
import React from 'react';
import LoadingScreen from 'components/LoadingScreen';
import { GET_TYPE } from './queries';
import TypeEditorForm from './TypeEditorForm';

const CreatorForm = () => <TypeEditorForm />;
const EditorForm = ({ id }) => {
  const { data, loading, error } = useQuery(GET_TYPE, { variables: { id }, fetchPolicy: 'no-cache' });

  if (error || !id) return <NotFound />;
  if (loading || !data) return <LoadingScreen />;

  return <TypeEditorForm initialType={data.getType} />;
};

const TypeEditor = ({ id }) => (id ? <EditorForm id={id} /> : <CreatorForm />);

export default TypeEditor;
