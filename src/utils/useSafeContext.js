import { useContext } from 'react';

const useSafeContext = (newContext, contextName) => {
  const context = useContext(newContext);
  if (!context) {
    throw new Error(
      `${contextName} hooks must be used within a ${contextName} Provider`,
    );
  }
  return context;
};

export default useSafeContext;
