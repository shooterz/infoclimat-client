import { useState } from 'react';

const getType = (variable) => {
  if (typeof variable === 'object' && Array.isArray(variable)) { return 'array'; }
  return typeof variable;
};

const memorize = (name, value) => window.localStorage.setItem(name, JSON.stringify({ typeof: getType(value), value }));

const read = (name, defaultValue) => {
  try {
    const storedValue = JSON.parse(window.localStorage.getItem(name));
    return storedValue?.value || defaultValue;
  } catch (err) {
    return defaultValue;
  }
};

const usePersistentState = (name, defaultValue) => {
  const usedDefaultValue = read(name, defaultValue);
  const [value, _setValue] = useState(usedDefaultValue);
  memorize(name, usedDefaultValue);

  const setValue = (newValue) => {
    memorize(name, newValue);
    _setValue(newValue);
  };

  return [value, setValue];
};

export default usePersistentState;
