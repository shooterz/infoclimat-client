import { useSnackbar } from 'notistack';
import APIError from './APIError';

const useAPIError = () => {
  const { enqueueSnackbar } = useSnackbar();
  const handleError = (error) => {
    /**
     * Check error type
     */
    if (error instanceof APIError) {
      // Display a snackbar if the error is not marked as silent
      if (error.getShouldDisplaySnackbar()) {
        enqueueSnackbar(error.getMessage(), { variant: 'error', autoHideDuration: 3000 });
      }
    } else {
      // Do something with others errors ...
    }
  };
  return { handleError };
};

export default useAPIError;
