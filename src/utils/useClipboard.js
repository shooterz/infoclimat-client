const { useSnackbar } = require('notistack');

const useClipboard = () => {
  const { enqueueSnackbar } = useSnackbar();

  const copy = (text) => {
    const el = document.createElement('textarea');
    el.value = text;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    enqueueSnackbar('Successfully copied!', {
      autoHideDuration: 3000,
    });
  };

  const paste = () => {
    document.execCommand('paste');
  };

  return { copy, paste };
};

export default useClipboard;
