const createReducer = (reducers) => (state, action) => reducers[action.type](state, action);

export default createReducer;
