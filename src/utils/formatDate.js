import moment, { HTML5_FMT } from 'moment';

const formatDisplayDateTime = (date) => moment(date).format('DD MMM, YYYY hh:mm');
const formatDisplayDate = (date) => moment(date).format('DD MMM YYYY');
const formatInputDateTime = (date) => moment(date).format(HTML5_FMT.DATETIME_LOCAL);
const formatInputDate = (date) => moment(date).format(HTML5_FMT.DATE);

export {
  formatDisplayDateTime,
  formatInputDateTime,
  formatDisplayDate,
  formatInputDate,
};
