class APIError extends Error {
  constructor(message, statusCode, shouldDisplaySnackbar = true) {
    super(message);
    this.statusCode = statusCode;
    this.shouldDisplaySnackbar = shouldDisplaySnackbar;
  }

  getMessage() {
    return this.message;
  }

  getShouldDisplaySnackbar() {
    return this.shouldDisplaySnackbar;
  }

  getStatusCode() {
    return this.statusCode;
  }
}

export default APIError;
