import config from 'config/index';
// import HTTPCodes from 'constants/HTTPCodes';
import qs from 'querystring';
import APIError from './APIError';

const { apiUrl } = config;

const defaultFetchOptions = {
  credentials: 'include',
  headers: {
    'Content-Type': 'application/json',
    Accept: '*/*',
  },
};

class Request {
  static get({
    route, queryParams, format, expectedErrors,
  }) {
    return Request.call({
      method: 'GET', route, queryParams, format, expectedErrors,
    });
  }

  static post({
    route, body, queryParams, format, expectedErrors,
  }) {
    return Request.call({
      method: 'POST', route, queryParams, body, format, expectedErrors,
    });
  }

  static put({
    route, body, queryParams, format, expectedErrors,
  }) {
    return Request.call({
      method: 'PUT', route, queryParams, body, format, expectedErrors,
    });
  }

  static push({
    route, body, format, expectedErrors,
  }) {
    return Request.call({
      method: 'PUSH', route, body, format, expectedErrors,
    });
  }

  static delete({ route, format, expectedErrors }) {
    return Request.call({
      method: 'DELETE', route, format, expectedErrors,
    });
  }

  static async call({
    method, route, queryParams, body, format = 'json', expectedErrors = [],
  }) {
    const allowedFormats = ['text', 'json'];

    if (!allowedFormats.find((f) => f === format)) {
      throw new Error(`Format is ${format} is forbidden.`);
    }

    const params = queryParams ? `?${qs.stringify(queryParams)}` : '';
    //
    const response = await fetch(`${apiUrl}${route}${params}`, {
      method,
      body: body ? JSON.stringify(body) : undefined,
      ...defaultFetchOptions,
    });

    if (response.status >= 400) {
      const matchError = expectedErrors?.find((e) => e.code === response.status);
      if (matchError) {
        // If message is set to null, explicitly means snackbar is disabled
        throw new APIError(matchError.message, matchError.code, matchError.message !== null);
      } else {
        throw new APIError('Server error.', response.status);
      }
    }

    try {
      const formatedResponse = await response[format]();
      return formatedResponse;
    } catch (error) {
      throw new APIError('Failed to read server response.', -1);
    }
  }
}

export default Request;
