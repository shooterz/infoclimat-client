const optionsToDisplay = (
  options,
  objFormat = { value: 'value', label: 'label' },
) => options.reduce(
  (acc, current) => ({
    ...acc,
    [current[objFormat.value]]: current[objFormat.label],
  }),
  {},
);

const selectOptions = (options, { value, label }) => options.reduce(
  (acc, current) => [
    ...acc,
    {
      value: current[value],
      label: current[label],
    },
  ],
  [],
);

export { selectOptions, optionsToDisplay };
