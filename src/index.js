import 'react-perfect-scrollbar/dist/css/styles.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import 'react-quill/dist/quill.snow.css';
import 'nprogress/nprogress.css';
import { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import StyledEngineProvider from '@material-ui/core/StyledEngineProvider';
import { SnackbarProvider } from 'notistack';
import { ApolloProvider } from '@apollo/client';
import apolloClient from 'external-services/apollo-client';
import ModeProvider from 'contexts/ModeContext';
import SwitchThemeProvider from 'contexts/ThemeContext';
import App from './App';
import * as serviceWorker from './serviceWorker';

const snackbarAnchorOrigin = {
  horizontal: 'center',
  vertical: 'bottom',
};

ReactDOM.render(
  <StrictMode>
    <StyledEngineProvider injectFirst>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <BrowserRouter>
          <SwitchThemeProvider>
            <SnackbarProvider
              maxSnack={3}
              anchorOrigin={snackbarAnchorOrigin}
            >
              <ModeProvider>
                <ApolloProvider client={apolloClient}>
                  <App />
                </ApolloProvider>
              </ModeProvider>
            </SnackbarProvider>
          </SwitchThemeProvider>
        </BrowserRouter>
      </LocalizationProvider>
    </StyledEngineProvider>
  </StrictMode>, document.getElementById('root'),
);

serviceWorker.register();
