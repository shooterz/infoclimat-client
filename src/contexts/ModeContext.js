import { createContext, useContext, useState } from 'react';

const MODES = {
  PRO: 'PRO',
  NOVICE: 'NOVICE',
};

const ModeContext = createContext();

const ModeProvider = ({ children }) => {
  const [mode, setMode] = useState(MODES.NOVICE);
  const toggleMode = () => setMode(mode === MODES.PRO ? MODES.NOVICE : MODES.PRO);

  return (
    <ModeContext.Provider value={{ mode, toggleMode }}>
      {children}
    </ModeContext.Provider>
  );
};

const useMode = () => useContext(ModeContext);

export { useMode, MODES };
export default ModeProvider;
