import { createContext, useContext, useState } from 'react';
import THEMES from 'constants/themes';
import { ThemeProvider } from '@material-ui/core';
import { createTheme } from 'theme';

const ThemeContext = createContext();

const SwitchThemeProvider = ({ children }) => {
  const [themeValue, setThemeValue] = useState(THEMES.NATURE);

  const theme = createTheme({
    direction: 'ltr',
    responsiveFontSizes: true,
    roundedCorners: false,
    theme: themeValue,
    primary: '#4B77AA',
  });

  const toggleTheme = () => {
    setThemeValue(themeValue === THEMES.NATURE ? THEMES.LIGHT : THEMES.NATURE);
  };

  return (
    <ThemeContext.Provider value={{ theme: themeValue, toggleTheme }}>
      <ThemeProvider theme={theme}>
        {children}
      </ThemeProvider>
    </ThemeContext.Provider>
  );
};

const useTheme = () => useContext(ThemeContext);

export { useTheme };
export default SwitchThemeProvider;
