const importances = {
  '-1': {
    label: 'Non concerné',
    color: '#ffffff',
  },
  0: {
    label: 'Non défini',
    color: '#ffffff',
  },
  1: {
    label: 'Anecdotique',
    color: '#33cccc',
  },
  2: {
    label: 'Notable',
    color: '#99cc00',
  },
  3: {
    label: 'Remarquable',
    color: '#ff9900',
  },
  4: {
    label: 'Exceptionnel',
    color: '#ff0000',
  },
  5: {
    label: 'Mémorable',
    color: '#ff00ff',
  },
  // 99: {
  //   label: 'dépression tropicale',
  //   color: '#ffffff',
  // },
  // 98: {
  //   label: 'tempête tropicale',
  //   color: '#ffffff',
  // },
  // 91: {
  //   label: 'cyclone cat. 1',
  //   color: '#33cccc',
  // },
  // 92: {
  //   label: 'cyclone cat. 2',
  //   color: '#99cc00',
  // },
  // 93: {
  //   label: 'cyclone cat. 3',
  //   color: '#ff9900',
  // },
  // 94: {
  //   label: 'cyclone cat. 4',
  //   color: '#ff0000',
  // },
  // 95: {
  //   label: 'cyclone cat. 5',
  //   color: '#ff00ff',
  // },
};

const getImportance = (importanceCode) => importances[importanceCode];

const getImportanceLite = (importanceCode) => {
  if (importanceCode <= 1) return 'Faible';
  if (importanceCode <= 3) return 'Moyen';
  return 'Fort';
};

export { getImportance };
export default importances;
