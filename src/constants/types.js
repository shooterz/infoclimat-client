import rain from '../images/rain.gif';
import storm from '../images/storm.gif';
import sun from '../images/sun.gif';
import wind from '../images/wind.gif';
import snow from '../images/snow.gif';
import cold from '../images/cold.gif';

const types = {
  1: { name: 'froid inhabituel', image: cold },
  2: { name: 'épisode neigeux', image: snow },
  3: { name: 'douceur inhabituelle', image: sun },
  4: { name: 'épisode pluvieux', image: rain },
  5: { name: 'inondation', image: rain },
  6: { name: 'tempête/coup de vent', image: wind },
  7: { name: 'cyclone', image: wind },
  8: { name: 'chaleur / canicule', image: sun },
  9: { name: 'orages', image: storm },
  10: { name: 'sécheresse', image: sun },
  11: { name: 'pluies verglaçantes', image: rain },
  12: { name: 'gelées tardives', image: snow },
  13: { name: 'gelées précoces', image: snow },
  14: { name: 'froid', image: cold },
  15: { name: 'grêle', image: snow },
  16: { name: 'épisode neigeux tardif', image: snow },
  17: { name: 'épisode neigeux précoce', image: snow },
  18: { name: 'tornade', image: wind },
  19: { name: 'dépression extra-tropicale', image: wind },
  20: { name: 'tempête tropicale', image: wind },
  '-1': { name: 'autres' },
};

const getType = (typesCodeSeparetedByComa) => {
  const typesCodes = typesCodeSeparetedByComa.split(',');
  return typesCodes.map((t) => types[t].name).join(', ');
};

const getImage = (typesCodeSeparetedByComa) => {
  const typesCodes = typesCodeSeparetedByComa.split(',');
  return typesCodes.map((t) => types[t].image);
};

export { getType, getImage };
export default types;
