const durees = {
  1: 'Ponctuel',
  2: 'Plusieurs jours',
  3: 'Plusieurs semaines',
  4: 'Plusieurs mois',
};

const getDuree = (dureeCode) => durees[dureeCode];

export { getDuree };
export default durees;
