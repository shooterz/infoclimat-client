const localisations = {
  42: 'Alsace',
  72: 'Aquitaine',
  83: 'Auvergne',
  26: 'Bourgogne',
  53: 'Bretagne',
  21: 'Champagne-Ardenne',
  94: 'Corse',
  43: 'Franche-Comté',
  11: 'Île-de-France',
  91: 'Languedoc-Roussillon',
  74: 'Limousin',
  24: 'Centre',
  41: 'Lorraine',
  73: 'Midi-Pyrénées',
  31: 'Nord-Pas-de-Calais',
  25: 'Basse-Normandie',
  23: 'Haute-Normandie',
  52: 'Pays de la Loire',
  22: 'Picardie',
  54: 'Poitou-Charentes',
  93: 'PACA',
  82: 'Rhône-Alpes',
};

const localisationsCode = {
  42: '67, 68',
  72: '24, 33, 40, 47, 64',
  83: '15, 43, 63, 03',
  26: '21, 58, 71, 89',
  53: '22, 29, 35, 56',
  21: '10, 51, 52, 08',
  94: '2A, 2B',
  43: '25, 39, 70, 90',
  11: '75, 91, 92, 93, 77, 94, 95, 78',
  91: '11, 30, 34, 48, 66',
  74: '19, 23, 87',
  24: '18, 28, 36, 37, 41, 45',
  41: '54, 55, 57, 88',
  73: '12, 31, 32, 46, 65, 81, 82, 09',
  31: '59, 62',
  25: '14, 50, 61',
  23: '27, 76',
  52: '44, 49, 53, 72, 85',
  22: '60, 80, 02',
  54: '16, 17, 79, 86',
  93: '13, 83, 84, 04, 05, 06',
  82: '26, 38, 42, 69, 73, 74, 01, 07',
};

const getLocalisations = (localisationsSeparatedByComa) => {
  const locs = localisationsSeparatedByComa.split(',');
  return locs.map((key) => localisations[key]).join(', ');
};

const getLocalisationsCode = (localisationsSeparatedByComa) => {
  const locs = localisationsSeparatedByComa.split(',');
  return locs.map((key) => localisationsCode[key]).join(', ');
};

const getLocalisationsArrayValues = (localisationsSeparatedByComa) => {
  if (localisationsSeparatedByComa === '-1') {
    return Object.keys(localisations).map((k) => ({
      key: k,
      name: localisations[k],
    }));
  }
  const locs = localisationsSeparatedByComa.split(',');
  return locs.map((key) => ({
    key,
    name: localisations[key],
  }));
};

const getLocalisationsDepartments = (localisationsSeparatedByComa) => {
  const departements = [];
  // Entire France
  if (localisationsSeparatedByComa === '-1') {
    const keys = Object.keys(localisationsCode);
    keys.forEach((key) => {
      const deps = localisationsCode[key].split(',');
      deps.forEach((dep) => departements.push(parseInt(dep, 10)));
      departements.push('2a');
      departements.push('2b');
    });
  } else {
    const locs = localisationsSeparatedByComa.split(',');
    locs.forEach((key) => {
      const deps = localisationsCode[key].split(',');
      deps.forEach((dep) => departements.push(parseInt(dep, 10)));
    });
  }
  return departements;
};

export {
  getLocalisations, getLocalisationsArrayValues, getLocalisationsCode, getLocalisationsDepartments,
};
export default localisations;
