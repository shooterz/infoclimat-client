import rain from '../images/rain.gif';
import storm from '../images/storm.gif';
import sun from '../images/sun.gif';
import wind from '../images/wind.gif';
import snow from '../images/snow.gif';
import cold from '../images/cold.gif';

const types = {
  1: { name: 'Température minimale', image: cold, unit: '°C' },
  2: { name: 'Précipitations / 24h (6h-6h)', image: rain, unit: 'mm' },
  3: { name: 'Vent en rafales', image: wind, unit: 'km/h' },
  4: { name: 'Hauteur de neige au sol', image: snow, unit: 'cm' },
  5: { name: 'Hauteur d’eau', image: rain, unit: 'm' },
  6: { name: 'Température maximale', image: sun, unit: '°C' },
  7: { name: 'Vent moyen', image: wind, unit: 'km/h' },
  8: { name: 'Hauteur de neige fraîche', image: snow, unit: 'cm' },
  9: { name: 'Précipitations / + de 48h', image: rain, unit: 'mm' },
  10: { name: 'Windchill', image: '', unit: '' },
  11: { name: 'Humidex', image: '', unit: '' },
  12: { name: 'Pression', image: wind, unit: 'hPa' },
  13: { name: 'Impacts de foudre', image: storm, unit: '' },
  14: { name: 'Taille des grêlons', image: snow, unit: 'cm' },
  15: { name: 'Précipitations / 48h (6h-6h)', image: rain, unit: 'mm' },
  16: { name: 'Précips. sur tout l’épisode', image: rain, unit: 'mm' },
  17: { name: 'Précipitations / 1h', image: rain, unit: 'mm' },
  18: { name: 'Précipitations / 6h', image: rain, unit: 'mm' },
  19: { name: 'Précipitations / 12h', image: rain, unit: 'mm' },
};

const getHValue = (eventDetail) => {
  if (eventDetail.historicValues.length === 0) return '';
  const historicValuesType = eventDetail.historicValues[0].type;
  return types[historicValuesType];
};

const getValueType = (type) => types[type];

export { getHValue, getValueType };
export default types;
