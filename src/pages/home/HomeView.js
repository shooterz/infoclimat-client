import HistoriEventsList from 'domains/HistoricEvents/List';
import Page from 'components/pages/Page';

const HomeView = () => (
  <Page title="HistorIC">
    <HistoriEventsList />
  </Page>
);

export default HomeView;
