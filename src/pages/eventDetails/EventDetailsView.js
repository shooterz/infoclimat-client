import { Box, Container } from '@material-ui/core';
import Page from 'components/pages/Page';
import { useNavigate, useParams } from 'react-router';
import { useQuery } from '@apollo/client';
import GET_EVENT_BY_ID from './query';
import KPI from '../../domains/HistoricEventDetails/KPI';
import BasicInformation from '../../domains/HistoricEventDetails/BasicInformation';
import EventDetailHeader from '../../domains/HistoricEventDetails/EventDetailHeader';

const EventDetailsView = () => {
  const { eventId } = useParams();
  const { data, loading, error } = useQuery(GET_EVENT_BY_ID, {
    variables: { id: parseInt(eventId, 10) },
    fetchPolicy: 'no-cache',
  });

  if (!data || loading || error) return null;

  const eventDetail = data?.getHistoricEventById;

  return (
    <Page title="HistorIC - Event Details">
      <Container maxWidth="lg">
        <Box
          component="span"
          m={1}
          backgroundColor="background.default"
          minHeight="100%"
          py={8}
        >
          <EventDetailHeader eventDetail={eventDetail} />
          <KPI eventDetail={eventDetail} />
          <BasicInformation eventDetail={eventDetail} />
        </Box>
      </Container>
    </Page>
  );
};

export default EventDetailsView;
