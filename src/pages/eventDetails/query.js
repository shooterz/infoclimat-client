import { gql } from '@apollo/client';

const GET_EVENT_BY_ID = gql`
    query getHistoricEventById($id: ID!) {
        getHistoricEventById(id: $id) {
            id
            nom
            localisation
            importance
            typeCyclone
            hasImageCyclone
            dateDeb
            dateFin
            duree
            type
            description
            shortDesc
            sources
            idCompte
            valeurMax
            bsLink
            genCartes
            why
            tableauCroise
            tableauCroiseCyclone
            hits
            notes
            historicValues {
                id
                lieu
                historicNormale {
                    geoid
                    mois
                    tx
                    tn
                    precip
                    altitudeRef
                    nomRef {
                        id
                    }
                    deptRef
                    distance
                    wmoRef
                }
                dept
                pays
                valeur
                type
                date
                commentaire
                estRecord
                estRecordDpt
            }
            historicCountValues
            historicMaxValues
            historicMinValues
            historicAvgValues
        }
    }
`;

export default GET_EVENT_BY_ID;
