import {
  TextField,
} from '@material-ui/core';
import React from 'react';
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types';

const Input = ({
  autoFocus,
  name,
  control,
  label,
  helperText,
  error,
  defaultValue,
  rules,
  ...props
}) => (
  <Controller
    name={name}
    control={control}
    defaultValue={defaultValue}
    rules={rules}
    render={({ field }) => (
      <TextField
        {...field}
        {...props}
        autoFocus={autoFocus}
        label={label}
        helperText={helperText}
        error={error}
        variant="outlined"
        margin="normal"
        size="small"
        fullWidth
      />
    )}
  />
);

Input.defaultProps = {
  helperText: undefined,
  error: false,
  autoFocus: false,
};

Input.propTypes = {
  autoFocus: PropTypes.bool,
  name: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  control: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  helperText: PropTypes.string,
  error: PropTypes.bool,
};

export default Input;
