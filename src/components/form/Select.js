import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select as MUISelect,
} from '@material-ui/core';
import React from 'react';
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types';

const Select = ({
  name,
  control,
  label,
  options,
  helperText,
  error,
  defaultValue,
  required,
  ...props
}) => (
  <Controller
    name={name}
    control={control}
    defaultValue={defaultValue}
    render={({ field }) => (
      <FormControl
        name={name}
        error={error}
        variant="outlined"
        margin="normal"
        size="small"
        fullWidth
        required={required}
      >
        {label && <InputLabel>{label}</InputLabel>}
        <MUISelect
          name={name}
          {...field}
          {...props}
          label={label}
          margin="normal"
        >
          {(options || []).map(({ label: optionLabel, value }) => (
            <MenuItem value={value} key={value}>
              {optionLabel}
            </MenuItem>
          ))}
        </MUISelect>
        {helperText && <FormHelperText>{helperText}</FormHelperText>}
      </FormControl>
    )}
  />
);

Select.defaultProps = {
  helperText: undefined,
  error: false,
};

Select.propTypes = {
  name: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  control: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ),
  helperText: PropTypes.string,
  error: PropTypes.bool,
};

export default Select;
