import {
  Checkbox as CheckboxMUI,
  FormControl,
  FormControlLabel,
  FormHelperText,
} from '@material-ui/core';
import React from 'react';
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types';

const Checkbox = ({
  name,
  control,
  label,
  helperText,
  error,
  icon,
  defaultChecked,
  ...props
}) => (
  <FormControl size="small" error={error} fullWidth>
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <FormControlLabel
          sx={{ '& > svg': { mr: 1 } }}
          control={(
            <>
              <CheckboxMUI
                color="primary"
                checked={field.value}
                {...field}
                {...props}
                defaultChecked={defaultChecked}
              />
              {icon}
            </>
          )}
          label={label}
        />
      )}
    />
    {helperText && <FormHelperText>{helperText}</FormHelperText>}
  </FormControl>
);

Checkbox.defaultProps = {
  helperText: undefined,
  icon: undefined,
  error: false,
};

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  icon: PropTypes.node,
  // eslint-disable-next-line react/forbid-prop-types
  control: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  helperText: PropTypes.string,
  error: PropTypes.bool,
};

export default Checkbox;
