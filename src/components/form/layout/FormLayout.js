import {
  AppBar, Box, Button, experimentalStyled, IconButton, Toolbar, Typography,
} from '@material-ui/core';
import ArrowLeftIcon from 'icons/ArrowLeft';
import React from 'react';
import { useNavigate } from 'react-router-dom';

const Wrapper = experimentalStyled('form')({
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
});

const Content = experimentalStyled(Box)({
  flex: 1,
});

const Footer = experimentalStyled(AppBar)({
  position: 'sticky',
  bottom: 0,
  left: 0,
  width: '100%',
});

const FormLayout = ({ title, onSubmit, children }) => {
  const navigate = useNavigate();
  return (
    <>
      <Wrapper onSubmit={onSubmit}>
        <AppBar color="default" position="sticky" elevation={0}>
          <Toolbar variant="dense">
            <IconButton onClick={() => navigate(-1)}>
              <ArrowLeftIcon />
            </IconButton>
            <Typography noWrap>
              {title}
            </Typography>
          </Toolbar>
        </AppBar>
        <Content sx={{ p: 2 }}>
          {children}
        </Content>
        <Footer color="default">
          <Toolbar
            sx={{
              justifyContent: 'center', backgroundColor: 'background.paper', borderTop: '1px solid', borderColor: 'divider',
            }}
            variant="dense"
          >
            <Button type="button" onClick={() => navigate(-1)} color="error" sx={{ mr: 2 }}>
              Cancel
            </Button>
            <Button type="submit" color="success" variant="outlined">
              Save
            </Button>
          </Toolbar>
        </Footer>
      </Wrapper>
    </>
  );
};

export default FormLayout;
