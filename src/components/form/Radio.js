import {
  Radio as RadioMUI,
  FormControl,
  FormControlLabel,
  FormHelperText,
} from '@material-ui/core';
import React from 'react';
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types';

const Radio = ({
  name,
  control,
  label,
  helperText,
  error,
  icon,
  ...props
}) => (
  <FormControl size="small" error={error} fullWidth>
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <FormControlLabel
          sx={{ '& > svg': { mr: 1 } }}
          {...field}
          {...props}
          value={field.value}
          control={(
            <>
              <RadioMUI
                color="primary"
                {...field}
                {...props}
              />
              {icon}
            </>
          )}
          label={label}
        />
      )}
    />
    {helperText && <FormHelperText>{helperText}</FormHelperText>}
  </FormControl>
);

Radio.defaultProps = {
  helperText: undefined,
  icon: undefined,
  error: false,
};

Radio.propTypes = {
  name: PropTypes.string.isRequired,
  icon: PropTypes.node,
  // eslint-disable-next-line react/forbid-prop-types
  control: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  helperText: PropTypes.string,
  error: PropTypes.bool,
};

export default Radio;
