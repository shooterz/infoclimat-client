import { useEffect } from 'react';
import NProgress from 'nprogress';
import { Box, CircularProgress } from '@material-ui/core';

const LoadingScreen = () => {
  useEffect(() => {
    NProgress.start();

    return () => {
      NProgress.done();
    };
  }, []);

  return (
    <Box
      sx={{
        backgroundColor: 'background.paper',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '100%',
      }}
    >
      <CircularProgress size={20} />
    </Box>
  );
};

export default LoadingScreen;
