import { PropTypes } from 'prop-types';
import { useEffect } from 'react';

const Page = ({
  title, children,
}) => {
  useEffect(() => {
    document.title = `${title} - infoclimat.fr`;
  }, [title]);

  return children;
};

Page.defaultProps = {
  children: null,
  inTab: false,
  tabName: undefined,
  hideTabs: false,
};

Page.propTypes = {
  title: PropTypes.string.isRequired,
  inTab: PropTypes.bool,
  tabName: PropTypes.string,
  hideTabs: PropTypes.bool,
  children: PropTypes.node,
};

export default Page;
