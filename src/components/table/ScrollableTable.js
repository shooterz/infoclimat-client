import {
  Box,
  Table,
  TableHead,
  TableBody,
  Card,
  CircularProgress,
  makeStyles,
  Toolbar,
  experimentalStyled,
} from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  flexColumn: {
    flex: 1, display: 'flex', flexDirection: 'column',
  },
});

const ActionsBar = experimentalStyled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  borderTop: '1px solid rgba(145, 158, 171, 0.24)',
  position: 'absolute',
  width: '100%',
  left: 0,
  bottom: 0,
  borderBottomLeftRadius: theme.shape.borderRadius,
  borderBottomRightRadius: theme.shape.borderRadius,
}));

const ScrollableTable = ({
  TableHeadComponent,
  TableBodyComponent,
  TablePaginationComponent,
  TableActionsComponent,
  showActions,
  loading,
}) => {
  const { flexColumn } = useStyles();

  return (
    <Card
      sx={{
        backgroundColor: 'background.paper',
        overflow: 'auto',
        '&::-webkit-scrollbar': {
          backgroundColor: 'transparent',
          width: 0,
          height: 0,
        },
        '&::-webkit-scrollbar-corner': {
          backgroundColor: 'transparent',
        },
      }}
      className={flexColumn}
    >
      <Box sx={{ minWidth: 1200 }} className={flexColumn}>
        <Table
          size="small"
          sx={{ maxHeight: '1px', paddingBottom: '64px' }}
          stickyHeader
        >
          <TableHead
            sx={{
              '& .MuiTableCell-root': {
                backgroundColor: 'background.paper',
              },
            }}
          >
            {TableHeadComponent}
          </TableHead>
          <TableBody>{TableBodyComponent}</TableBody>
        </Table>
      </Box>
      <ActionsBar
        sx={{
          backgroundColor: showActions
            ? 'background.default'
            : 'background.paper',
        }}
      >
        <Toolbar sx={{ minHeight: '64px', width: '100%' }}>
          {showActions
            ? TableActionsComponent
            : TablePaginationComponent}
          {loading && <CircularProgress size={20} />}
        </Toolbar>
      </ActionsBar>
    </Card>
  );
};

ScrollableTable.defaultProps = {
  TablePaginationComponent: null,
  loading: false,
};

ScrollableTable.propTypes = {
  TableHeadComponent: PropTypes.node.isRequired,
  TableBodyComponent: PropTypes.node.isRequired,
  TablePaginationComponent: PropTypes.node,
  loading: PropTypes.bool,
};

export default ScrollableTable;
