import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box, Button, Card, Collapse, Divider, Drawer, IconButton, Typography,
} from '@material-ui/core';
import ChevronDownIcon from 'icons/ChevronDown';
import ChevronRightIcon from 'icons/ChevronRight';
import useScreenType from 'theme/useScreenType';
import SearchIcon from 'icons/Search';
import CloseIcon from 'icons/Close';
import FiltersIcon from 'icons/Filters';

const TableFiltersTitle = ({
  title,
  isMobile,
  isOpen,
  setIsOpen,
}) => {
  const CloseFiltersIcon = isMobile
    ? CloseIcon
    : ChevronRightIcon;
  const OpenFiltersIcon = isMobile
    ? FiltersIcon
    : ChevronDownIcon;

  return (
    <Box
      sx={{
        m: 2,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <SearchIcon sx={{ mr: 1 }} />
        <Typography alignItems="center" color="textPrimary">
          {title}
        </Typography>
      </Box>
      <IconButton size="small" onClick={() => setIsOpen(!isOpen)}>
        {isOpen ? <CloseFiltersIcon /> : <OpenFiltersIcon />}
      </IconButton>
    </Box>
  );
};

const FiltersForm = ({ handleSubmit, handleReset, content }) => (
  <form onSubmit={handleSubmit}>
    <Divider />
    <Box sx={{ mx: 2, mb: 1 }}>
      {content}
    </Box>
    <Divider />
    <Box sx={{
      m: 2,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}
    >
      <Button size="small" onClick={handleReset} type="reset" color="success" sx={{ mr: 1 }} variant="outlined">
        Réinitialiser
      </Button>
      <Button size="small" type="submit" variant="outlined">
        Valider
      </Button>
    </Box>
  </form>
);

const MobileFilters = ({
  title,
  isMobile,
  isOpen,
  setIsOpen,
  handleSubmit,
  handleReset,
  content,
}) => (
  <Drawer anchor="top" open={isOpen} onClose={() => setIsOpen(false)}>
    <TableFiltersTitle
      title={title}
      isMobile={isMobile}
      isOpen={isOpen}
      setIsOpen={setIsOpen}
    />
    <FiltersForm
      handleSubmit={handleSubmit}
      handleReset={handleReset}
      content={content}
    />
  </Drawer>
);

const DesktopFilters = ({
  handleSubmit, handleReset, content, isOpen,
}) => (
  <Collapse in={isOpen}>
    <FiltersForm
      handleSubmit={handleSubmit}
      handleReset={handleReset}
      content={content}
    />
  </Collapse>
);

const TableFilters = ({
  title, onSubmit, onReset, children,
}) => {
  const { isMobile } = useScreenType();
  const [isOpen, setIsOpen] = useState(false);

  const handleSubmit = (e) => {
    onSubmit(e);
    // Close menu after action triggered on mobile
    if (isMobile) setIsOpen(false);
  };

  const handleReset = (e) => {
    onReset(e);
    // Close menu after action triggered on mobile
    if (isMobile) setIsOpen(false);
  };

  return (
    <Card>
      <TableFiltersTitle
        title={title}
        isMobile={isMobile}
        isOpen={isOpen}
        setIsOpen={setIsOpen}
      />
      {isMobile
        ? (
          <MobileFilters
            title={title}
            isMobile={isMobile}
            isOpen={isOpen}
            setIsOpen={setIsOpen}
            handleSubmit={handleSubmit}
            handleReset={handleReset}
            content={children}
          />
        )
        : (
          <DesktopFilters
            isOpen={isOpen}
            handleSubmit={handleSubmit}
            handleReset={handleReset}
            content={children}
          />
        )}
    </Card>
  );
};

TableFilters.propTypes = {
  title: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default TableFilters;
