import { Button } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';
import useScreenType from 'theme/useScreenType';

const TableActionButton = ({
  color, startIcon, children, onClick, ...props
}) => {
  const { isTablet } = useScreenType();
  const variant = isTablet ? undefined : 'outlined';
  const size = isTablet ? undefined : 'small';

  return (
    <Button size={size} color={color} variant={variant} startIcon={startIcon} onClick={onClick} {...props}>
      {children}
    </Button>
  );
};

TableActionButton.defaultProps = {
  color: 'primary',
  onClick: undefined,
};

TableActionButton.propTypes = {
  color: PropTypes.string,
  startIcon: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  children: PropTypes.node.isRequired,
};

export default TableActionButton;
