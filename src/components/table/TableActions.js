import {
  Box, Drawer, IconButton, Typography,
} from '@material-ui/core';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import useScreenType from 'theme/useScreenType';
import MoreIcon from 'icons/More';

const TableActions = ({ selectedRows, children }) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const { isTablet } = useScreenType();
  return (
    <Box sx={{
      display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%',
    }}
    >
      <Typography color="textPrimary">
        {`${selectedRows} selectionnée(s)`}
      </Typography>
      <Box sx={{ display: 'flex', '& > button:not(button:first-of-type)': { ml: 1 } }}>
        {isTablet ? (
          <IconButton onClick={() => setMenuOpen(!menuOpen)} size="small">
            <MoreIcon />
          </IconButton>
        ) : children}
      </Box>
      {isTablet && (
        <Drawer anchor="bottom" open={menuOpen} onClose={() => setMenuOpen(false)}>
          <Box m={2} mb={0}>
            <Typography color="textPrimary" variant="subtitle2">
              {`${selectedRows} selectionnée(s)`}
            </Typography>
          </Box>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              '& > button': { mt: 2, mx: 2 },
              '& > button:last-of-type': { mb: 2 },
            }}
            onClick={() => setMenuOpen(false)}
          >
            {children}
          </Box>
        </Drawer>
      )}
    </Box>
  );
};

TableActions.propTypes = {
  selectedRows: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
};

export default TableActions;
