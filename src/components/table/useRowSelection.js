import { useState } from 'react';

const useRowSelection = () => {
  const [selected, setSelected] = useState([]);

  const onSelectRow = (id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const unselectAll = () => setSelected([]);

  const isSelected = (id) => selected.indexOf(id) !== -1;

  return {
    selected, onSelectRow, isSelected, unselectAll,
  };
};

export default useRowSelection;
