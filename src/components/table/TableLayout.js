import { Box, Container } from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';

const TableLayout = ({ FiltersComponent, TableComponent }) => (
  <Box
    sx={{
      backgroundColor: 'background.default',
      minHeight: '100%',
      display: 'flex',
      flexDirection: 'column',
      pb: 3,
    }}
  >
    <Container maxWidth="xl" sx={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
      <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
        {FiltersComponent && (
          <Box sx={{ mt: 3, flex: 0 }}>
            {FiltersComponent}
          </Box>
        )}
        <Box sx={{
          mt: 3, flex: 1, display: 'flex', flexDirection: 'column', position: 'relative',
        }}
        >
          {TableComponent}
        </Box>
      </Box>
    </Container>
  </Box>
);

TableLayout.defaultProps = {
  FiltersComponent: null,
};

TableLayout.propTypes = {
  FiltersComponent: PropTypes.node,
  TableComponent: PropTypes.node.isRequired,
};

export default TableLayout;
