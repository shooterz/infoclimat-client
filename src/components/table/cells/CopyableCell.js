import {
  Box, experimentalStyled, IconButton, TableCell,
} from '@material-ui/core';
import ClipboardIcon from 'icons/Clipboard';
import PropTypes from 'prop-types';
import React from 'react';
import useClipboard from 'utils/useClipboard';

const StyledCell = experimentalStyled(TableCell)({
  position: 'relative',
  '& .copy-to-clipboard': {
    position: 'absolute',
    right: '0px',
    display: 'none',
  },
  '&:hover .copy-to-clipboard': {
    display: 'block',
  },
});

const CellContent = experimentalStyled(Box)({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const CopyableCell = ({ children }) => {
  const { copy } = useClipboard();

  const copyCellContent = (e) => {
    e.preventDefault();
    e.stopPropagation();
    copy(children);
  };

  return (
    <StyledCell>
      <CellContent>
        {children}
        <IconButton onClick={copyCellContent} className="copy-to-clipboard" size="small">
          <ClipboardIcon fontSize="small" />
        </IconButton>
      </CellContent>
    </StyledCell>
  );
};

CopyableCell.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CopyableCell;
