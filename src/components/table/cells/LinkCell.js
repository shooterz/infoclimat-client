import { experimentalStyled, TableCell } from '@material-ui/core';
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

const StyledLink = experimentalStyled(RouterLink)(({ theme }) => ({
  display: 'block',
  color: theme.palette.primary.main,
}));

const LinkCell = ({ children, to }) => (
  <TableCell>
    <StyledLink to={to}>
      {children}
    </StyledLink>
  </TableCell>
);

export default LinkCell;
