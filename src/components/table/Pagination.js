import {
  Toolbar, Pagination as MuiPagination,
} from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';
import useScreenType from 'theme/useScreenType';

const Pagination = ({
  count, page, rowsPerPage, onPageChange,
}) => {
  const { isMobile } = useScreenType();
  return (
    <Toolbar disableGutters sx={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
      <MuiPagination
        color="primary"
        variant="outlined"
        shape="rounded"
        size="small"
        count={Math.ceil(count / rowsPerPage)}
        page={page + 1}
        onChange={(e, p) => onPageChange(p - 1)}
        siblingCount={isMobile ? 0 : 1}
        showFirstButton
        showLastButton
      />
    </Toolbar>
  );
};

Pagination.propTypes = {
  count: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
