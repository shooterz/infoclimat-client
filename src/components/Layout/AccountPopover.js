import { useMemo, useRef, useState } from 'react';
import {
  Avatar,
  Box,
  ButtonBase,
  Divider,
  Popover,
  Switch,
  Typography,
} from '@material-ui/core';
import { MODES, useMode } from 'contexts/ModeContext';
import { useTheme } from 'contexts/ThemeContext';
import THEMES from 'constants/themes';

const AccountPopover = () => {
  const anchorRef = useRef(null);
  const [open, setOpen] = useState(false);
  const { mode, toggleMode } = useMode();
  const { theme, toggleTheme } = useTheme();

  const switchLabel = useMemo(() => `Mode ${mode === MODES.NOVICE ? 'Lite' : 'Pro'}`, [mode]);
  const switchLabelTheme = useMemo(() => `Theme ${theme === THEMES.NATURE ? 'Dark' : 'Light'}`, [theme]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Box
        component={ButtonBase}
        onClick={handleOpen}
        ref={anchorRef}
        sx={{
          alignItems: 'center',
          display: 'flex',
        }}
      >
        <Avatar
          sx={{
            height: 32,
            width: 32,
          }}
        />
      </Box>
      <Popover
        anchorEl={anchorRef.current}
        anchorOrigin={{
          horizontal: 'center',
          vertical: 'bottom',
        }}
        getContentAnchorEl={null}
        keepMounted
        onClose={handleClose}
        open={open}
        PaperProps={{
          sx: { width: 240 },
        }}
      >
        <Box sx={{ p: 2 }}>
          <Typography
            color="textPrimary"
            variant="subtitle2"
          >
            Madame MICHU
          </Typography>
          <Typography
            color="textSecondary"
            variant="subtitle2"
          >
            madame.michu@gmail.fr
          </Typography>
        </Box>
        <Divider />
        <Box sx={{
          p: 2, display: 'flex', alignItems: 'center', flexDirection: 'column',
        }}
        >
          <Typography>
            {switchLabel}
          </Typography>
          <Switch color="primary" checked={mode === MODES.PRO} onChange={toggleMode} />
          <Typography>
            {switchLabelTheme}
          </Typography>
          <Switch color="primary" checked={theme === THEMES.NATURE} onChange={toggleTheme} />
        </Box>
      </Popover>
    </>
  );
};

export default AccountPopover;
