import { Box } from '@material-ui/core';
import logo from 'images/logo.webp';

const Logo = () => (
  <Box>
    <img alt="Infoclimat Logo" height="24px" width="auto" src={logo} />
  </Box>
);

export default Logo;
