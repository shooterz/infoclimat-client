import { experimentalStyled } from '@material-ui/core';
import React from 'react';

const StyledLink = experimentalStyled('a')(({ theme }) => ({
  color: theme.palette.primary,
}));

const ExternalLink = ({ href, children }) => (
  <StyledLink href={href}>{children}</StyledLink>
);

export default ExternalLink;
