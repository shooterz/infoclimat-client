import { useRoutes } from 'react-router-dom';
import './translations/i18n';
import React from 'react';
import useScrollReset from 'utils/useScrollReset';
import moment from 'moment';
import GlobalStyles from './components/GlobalStyles';
import routes from './routes';

moment.locale('fr');

const App = () => {
  const content = useRoutes(routes);
  useScrollReset();

  return (
    <>
      <GlobalStyles />
      {content}
    </>
  );
};

export default App;
