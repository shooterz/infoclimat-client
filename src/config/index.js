import JSONConfig from './config.json';

const env = process.env.NODE_ENV;

const config = JSONConfig[env];

export default config;
